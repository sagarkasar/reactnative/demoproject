import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'
import { Homestyles } from './Home'


const images = [
  require('../Assets/Images/1.jpg'),
  require('../Assets/Images/3.jpg')
];
const About = ({ navigation }) => {
  
  return (
    <View style={Homestyles.Container}>
     
        <TouchableOpacity onPress={()=>navigation.navigate('Home')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>navigation.navigate('Contact')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
    </View>
  )
}

export default About

const styles = StyleSheet.create({
 
})