import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React, {useState} from 'react'
import { Homestyles } from './Home'
import { SliderBox } from "react-native-image-slider-box";

const Contact = ({ navigation, props }) => {
    const [SlideImages, setSlideImages] = useState('')
    
    const showImages = {
        images: [
            require('../Assets/Images/1.jpg'),
            require('../Assets/Images/3.jpg'),
            require('../Assets/Images/5.jpg')
        ]
    }
  return (
      <View style={Homestyles.Container}>
          
          <SliderBox
            images={showImages.images}
            sliderBoxHeight={'100%'}
            />
        <TouchableOpacity onPress={()=>navigation.navigate('Home')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>navigation.navigate('About')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
    </View>
  )
}

export default Contact

const styles = StyleSheet.create({

})