import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'

const Home = ({navigation}) => {
  return (
    <View style={Homestyles.Container}>
        <TouchableOpacity onPress={()=>navigation.navigate('About')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>navigation.navigate('Contact')}>
            <Text style={Homestyles.btn}>Next Page</Text>
        </TouchableOpacity>
    </View>
  )
}

export default Home

export const Homestyles = StyleSheet.create({
    Container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
    },
    btn: {
        color: 'black',
        backgroundColor: '#d1a573',
        fontSize: 14,
        fontWeight:'700',
        padding: 5,
        alignItems: 'center',
        textAlign: 'center',
        borderRadius: 10,
        width: 150,
        borderColor: 'black',
        borderWidth: 1
    }
})